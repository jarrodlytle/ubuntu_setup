#INSTRUCTIONS
##################################################
# 1. Copy this file into your new ubuntu 13 isntallation
# 2. Give the file execute permissions: chmod +x [filename]
# 3. Execute the file: bash [filename]


#REPOSITORIES
##################################################
sudo add-apt-repository ppa:webupd8team/java #TODO find a better repo for jdk, this is horribly slow (an hour for 100MB)
sudo add-apt-repository ppa:richarvey/nodejs
sudo add-apt-repository ppa:olivier-berten/misc
sudo add-apt-repository ppa:otto-kesselgulasch/gimp
sudo add-apt-repository ppa:mozillateam/firefox-next

#UPDATE/UPGRADE REPOISTOIES AND OS
##################################################
sudo apt-get update
sudo apt-get upgrade


#PACKAGES
##################################################
sudo apt-get install software-properties-common
sudo apt-get install python-software-properties
sudo apt-get install build-essential
sudo apt-get install wget
sudo apt-get install vim
sudo apt-get install curl
sudo apt-get install tasksel
sudo apt-get install git-core
sudo apt-get install ruby-full
sudo apt-get install rubygems
sudo apt-get install nodejs
sudo tasksel install lamp-server
sudo apt-get install php5
sudo apt-get install php5-dev
sudo apt-get install php5-mcrypt
sudo apt-get install php5-mhash
sudo apt-get install php5-gd
sudo apt-get install php5-curl
sudo apt-get install php5-xdebug
sudo apt-get install php5-pear
sudo apt-get purge openjdk*
sudo apt-get install oracle-java8-installer
sudo apt-get install mysql-workbench
sudo apt-get install gimp
sudo apt-get install gimp-plugin-registry
sudo apt-get install gimp-gmic
sudo apt-get install firefox
sudo gem install sass


#SOFTWARE
#################################################
wget -O ~/tmp/google-chrome-stable_current_amd64.deb https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo dpkg -i ~/tmp/google-chrome-stable_current_amd64.deb
sudo apt-get -f install
rm ~/tmp/google-chrome-stable_current_amd64.deb

wget -O ~/Downloads/PhpStorm-7.0.tar.gz http://download.jetbrains.com/webide/PhpStorm-7.0.tar.gz
tar zxf ~/Downloads/PhpStorm-7.0.tar.gz -C ~/Downloads/
rm ~/Downloads/PhpStorm-7.0.tar.gz
mkdir ~/PhpStorm
mv ~/Downloads/PhpStorm-*/* ~/PhpStorm
chmod +x ~/PhpStorm/bin/phpstorm.sh 
~/PhpStorm/bin/phpstorm.sh

##TODO check if /usr/local/bin/modgit already exists
curl https://raw.github.com/jreinke/modgit/master/modgit > modgit
chmod +x modgit
sudo mv modgit /usr/local/bin


#CREATE SHH KEY
##################################################
##TODO check if ~/.ssh/id_rsa already exists
read -p "Enter your email address/username for use inside your ssh key." sshuname
ssh-keygen -t rsa -C "$sshuname"


#SETUP APACHE CONFIGURATION
##################################################
sudo usermod -a -G www-data $USER
sudo chgrp -R www-data /var/www
sudo chmod -R g+rw /var/www
sudo chmod 2770 /var/www

#create/include apache2 conf files
echo "ServerName localhost" | sudo tee -a /etc/apache2/conf-available/servername.conf
sudo a2enconf servername
sudo a2enmod rewrite
sudo a2dissite 000-default
#sudo service apache2 restart


#CREATE .BASHRC
##################################################


#CREATE .BASH_ALIASES
##################################################
cat <<EOF> ~/.bash_aliases
	alias ls="ls -hal"
EOF


#CREATE LOCDEV FILE
##################################################
if [ ! -d ~/tmp ]; then
	mkdir ~/tmp
	echo "~/tmp created"
fi

##TODO check if ~/tmp/locdev already exists
cat <<- 'FIRST' > ~/tmp/localdev
	function init
	{
		if [ "$#" = "0" ]; then
			read -p "What would you like to do?
			createsite
			" ans1
			callfunction "$ans1"
		else
			callfunction "$1"
		fi
		#echo "$FUNC_NAME"
	}
	
	function callfunction {
		case "$1" in
           createsite) createsite;;
           * ) echo "Exiting, Next time choose an action"; exit;;
       esac
	}
	
	function createsite 
	{
		read -p "What should the local url be?" url
		if [ -d "$url" ]; then
           echo "ERROR: This environment already exists. Remove $url and run this script again."
           exit 5
		fi
		
		mkdir "/var/www/$url"
		mkdir "/var/www/$url/public"
		mkdir "/var/www/$url/logs"
		mkdir "/var/www/$url/backups"
		mkdir "/var/www/$url/private"
		echo "127.0.0.1 $url" | sudo tee -a /etc/hosts
		
		echo "Creating the vhost config file"
		cat <<-SECOND > ~/tmp/$url.conf
	<VirtualHost *:80>
		ServerName  $url
		ServerAlias $url

		DocumentRoot /var/www/$url/public
		LogLevel warn
		ErrorLog  /var/www/$url/logs/error.log
		CustomLog /var/www/$url/logs/access.log combined
		<Directory />
		        Options FollowSymLinks
		        AllowOverride None
		</Directory>
		<Directory /var/www/$url/public/>
		        Options Indexes FollowSymLinks MultiViews
		        AllowOverride None
		        Order allow,deny
		        allow from all
		</Directory>
	</VirtualHost>
	SECOND
		sudo mv ~/tmp/$url.conf /etc/apache2/sites-available/$url.conf
 

	cat <<-THIRD > /var/www/$url/public/index.php
		<html>
			<body>
				<h1>$url</h1>
			</body>
		</html>
	THIRD

		echo "enabling site"
		sudo a2ensite $url
 
		echo "restarting apache"
		sudo service apache2 restart

		echo "all set!!"
		exit 0
	}
	
	init
FIRST
chmod +x ~/tmp/localdev
##TODO check if /usr/local/bin/localdev already exists
sudo mv ~/tmp/localdev /usr/local/bin

#LOGOUT (to allow for user permission changes to take affect)
#################################################
#TODO figure out how to logout
echo "If the computer does not restart, Please restart manually for all changes to take affect."
sudo reboot